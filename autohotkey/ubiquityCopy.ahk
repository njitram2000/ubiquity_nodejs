; https://github.com/zhamlin/AHKhttp
; https://github.com/jleb/AHKsock

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#Persistent
#SingleInstance, force
SetBatchLines, -1

#Include, autohotkey/AHKhttp.ahk
#Include, autohotkey/AHKsock.ahk

paths := {}
paths["/"] := Func("Copy")

server := new HttpServer()
server.LoadMimes(A_ScriptDir . "/mime.types")
server.SetPaths(paths)
server.Serve(8000)
return

Copy(ByRef req, ByRef res) {
	clipsaved := ClipboardAll    ; This line is here so the original clipboard contents can be restored when the script is finished.
    Clipboard =    ; This erases the clipboard, so that we can be sure something new is added in the next step.
    Send, ^c    ; Add the highlighted text to the clipboard
    Sleep 150  ; Give Windows time to actually populate the clipboard - you may need to experiment with the time here.
    responseBody := Clipboard
    Clipboard := clipsaved    ; Sets the clipboard back to whatever was on it before you ran this script.

    res.SetBodyText(responseBody)
    res.status := 200
}
