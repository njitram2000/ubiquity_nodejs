// https://github.com/sindresorhus/open
const open = require('open');
// https://wilix-team.github.io/iohook/usage.html
const ioHook = require('iohook');

const { spawn } = require('child_process');
const ahk = spawn('./autohotkey/AutoHotkey.exe', ['./autohotkey/ubiquityCopy.ahk']);

// Opens the URL in a specified browser.
// await open('https://www.google.com', {app: 'chrome'});
var lastKeyStroke = [null, Date.now()];
ioHook.on('keydown', event => {
	// If not a repeat of the same keydown within 100 ms (windows bug)
	if(!(event.rawcode === lastKeyStroke[0] && Date.now() - lastKeyStroke[1] < 100)) {
		if(event.ctrlKey && event.rawcode === 32) {
			// var backup = clipboardy.readSync();
			runUbiquity();
		}
	}
	lastKeyStroke = [event.rawcode, Date.now()];
});
ioHook.start();

var runUbiquity = () => {
	console.log("hello");
	const request = require('request');
	
	var restUrl = 'http://localhost:4568/show?input=';
	var copyUrl = 'http://localhost:8000';
	request(copyUrl, {}, (err, res, body) => {
		request(restUrl + body, {}, () => {
		});
	});
};